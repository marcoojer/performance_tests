#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from performance_tests.msg import SuperAwesome
from dynamic_reconfigure.server import Server
from performance_tests.cfg import frecuency_changeConfig


def callback(config, level):
	global frecuency
	frecuency=config["Frecuency"]
	rospy.loginfo("New frecuency of 15 %f", frecuency*0.85)
	rospy.loginfo("New frecuency of 30 %f", frecuency*0.7)
	return config

def PublisherPy():
	global frecuency
	pubpy=rospy.Publisher('/blue_ocean',SuperAwesome,queue_size=10)
	rospy.init_node('PublisherPy',anonymous=True)
	srv=Server(frecuency_changeConfig, callback)
	msg=SuperAwesome()
	msg.fish="Clownfish"
	frecuency=10
	while not rospy.is_shutdown():
		r=rospy.Rate(frecuency) #10 Hz
		
		#frecuency=frecuency+5
		pubpy.publish(msg)

		r.sleep()

if __name__=='__main__':
	try:
		PublisherPy()
	except rospy.ROSInterruptException:
		pass
