#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from performance_tests.msg import SuperAwesome
from dynamic_reconfigure.server import Server
from performance_tests.cfg import window_changeConfig

def callback(config, level):
	global num_value
	num_value=config["window_size"]
	return config
def callbackmsg(msg):
	global iteration
	global mean
	global Tprev
	global num_value
	Tactual=rospy.get_time()
	time_elapsed=(1/(Tactual-Tprev))
	mean+=(1.0/num_value)*time_elapsed
	iteration+=1
	if iteration==num_value:
		rospy.loginfo("I received  a blue %s at %f",msg.fish,mean)	
		iteration=0
		mean=0	
	Tprev=Tactual
	

def SubscriberPy():
	rospy.init_node('SubscriberPy',anonymous=True)
	global Tprev
	global iteration
	global mean
	global num_value
	num_value=500
	Tprev=rospy.get_time()
	srv=Server(window_changeConfig, callback)
	rospy.Subscriber("/blue_ocean",SuperAwesome,callbackmsg)
	iteration=0
	mean=0
	rospy.spin()

if __name__=='__main__':
	SubscriberPy()
