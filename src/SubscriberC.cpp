#include <ros/ros.h>
#include "std_msgs/String.h"
#include "performance_tests/SuperAwesome.h"
#include "std_msgs/Float64.h"
#include <dynamic_reconfigure/server.h>
#include <performance_tests/window_changeConfig.h>
ros::Time Tprev;
ros::Time Tactual;


double time_elapsed;
int iteration=0;
double mean=0;
int num_value=500;
void sub_callback(const performance_tests::SuperAwesome & msg)
{

	Tactual=ros::Time::now();
	time_elapsed=1/(Tactual-Tprev).toSec();
	mean+=(1.0/num_value)*time_elapsed;
	iteration++;
	if (iteration==num_value){
		ROS_INFO("I Received a blue %s at %f  \n",msg.fish.c_str(),mean);
	mean=0;
	iteration=0;}	
	Tprev=Tactual;
}

void callback(performance_tests::window_changeConfig &config, uint32_t level) {

	num_value=config.window_size;	
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "SubscriberC");
	ros::NodeHandle n;
	
// Subscriber
	ros::Subscriber sub_c=n.subscribe("/blue_ocean",1,sub_callback);
	 Tprev = ros::Time::now();
	dynamic_reconfigure::Server<performance_tests::window_changeConfig> server;
 	dynamic_reconfigure::Server<performance_tests::window_changeConfig>::CallbackType f;
 
	 f = boost::bind(&callback, _1, _2);
 	 server.setCallback(f);

	ros::spin();	

	return 0;
}
