#include <ros/ros.h>
#include "std_msgs/String.h"
#include "performance_tests/SuperAwesome.h"
#include <dynamic_reconfigure/server.h>
#include <performance_tests/frecuency_changeConfig.h>
int frecuency=30;

void callback(performance_tests::frecuency_changeConfig &config, uint32_t level) {

	ROS_INFO("Frecuency limit 15 %f",config.Frecuency*0.85);
	ROS_INFO("Frecuency limit 30 %f",config.Frecuency*0.7);
	frecuency=config.Frecuency;
}


int main(int argc, char **argv)
{
	ros::init(argc, argv, "PublisherC");
	ros::NodeHandle n;

// Publisher
	ros::Publisher pub_c=n.advertise<performance_tests::SuperAwesome>("/blue_ocean",1);

  	dynamic_reconfigure::Server<performance_tests::frecuency_changeConfig> server;
 	dynamic_reconfigure::Server<performance_tests::frecuency_changeConfig>::CallbackType f;
 
	 f = boost::bind(&callback, _1, _2);
 	 server.setCallback(f);
	
	performance_tests::SuperAwesome msg;
	msg.fish="Shark";



	 while(ros::ok()){
		ros::Rate loop_rate(frecuency);
		pub_c.publish(msg);
		ros::spinOnce();
		loop_rate.sleep();
	}
 	
	return 0;
}
